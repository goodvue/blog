### 为什么要用elk
**一般我们需要进行日志分析场景：直接在日志文件中 grep、awk 就可以获得自己想要的信息。
但在规模较大的场景中，此方法效率低下，面临问题包括日志量太大如何归档、文本搜索太慢怎么办、如何多维度查询。需要集中化的日志管理，所有服务器上的日志收集汇总。常见解决思路是建立集中式日志收集系统，将所有节点上的日志统一收集，管理，访问。一般大型系统是一个分布式部署的架构，不同的服务模块部署在不同的服务器上，问题出现时，大部分情况需要根据问题暴露的关键信息，定位到具体的服务器和服务模块，构建一套集中式日志系统，可以提高定位问题的效率。**
---
一个完整的集中式日志系统，需要包含以下几个主要特点：

 * 收集－能够采集多种来源的日志数据
 * 传输－能够稳定的把日志数据传输到中央系统
* 存储－如何存储日志数据
* 分析－可以支持 UI 分析
* 警告－能够提供错误报告，监控机制

### ELK简介
ELK是三个开源软件的缩写，分别表示：Elasticsearch , Logstash, Kibana , 它们都是开源软件。新增了一个FileBeat，它是一个轻量级的日志收集处理工具(Agent)，Filebeat占用资源少，适合于在各个服务器上搜集日志后传输给Logstash，官方也推荐此工具。

Elasticsearch是个开源分布式搜索引擎，提供搜集、分析、存储数据三大功能。它的特点有：分布式，零配置，自动发现，索引自动分片，索引副本机制，restful风格接口，多数据源，自动搜索负载等。

### 安装Elasticesearch
```
cd /usr/local/elk
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.6.0-linux-x86_64.tar.gz
tar xvf elasticsearch-7.6.0-linux-x86_64.tar.gz

```

#### 启动es
使用默认配置,启动服务，不能使用root 用户
```php
cd /usr/local/elk/elasticsearch-7.6.0/bin
./elasticsearch -d
curl -XGET localhost:9200  // 查看安装是否成功


 //解决方法  创建elastic用户
adduser elastic
设置密码
passwd elastic
修改目录权限
chown -R  elstic /etc/usr/elk
切换用户
su elastic
```
各种错误 内存不够或者分配内存问题
```php
* 修改 jvm.options 内存配置为 512m
错误提示： the default discovery settings are unsuitable for production use; at least one of [discovery.seed_hosts, discovery.seed_providers, cluster.initial_master_nodes] must be configured  
处理：修改elasticsearch.yml配置文件   cluster.initial_master_nodes: ["node-1","node-2"]修改为：cluster.initial_master_nodes: ["node-1"]
错误提示： max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144] 
 处理：在/etc/sysctl.conf文件最后添加一行
   vm.max_map_count=262144
```


### 安装下 载安装启动kibana
```php

cd /usr/local/elk/
wget https://artifacts.elastic.co/downloads/kibana/kibana-7.6.0-linux-x86_64.tar.gz
tar xvf kibana-7.6.0-linux-x86_64.tar.gz
// 启动
cd  kibana-7.6.0-linux-x86_64 && ./kibana 

nohup /usr/local/kibana/bin/kibana &
```

#### 修改配置，使其能够外网访问，并且配置为中文版
```php
vim /usr/local/elk/kibana-7.6.0-linux-x86_64/config/kibana.xml

修改1. 改为外网可访问形式（不建议，最好限制ip）
		server.host: "0.0.0.0"
修改2. 最后一行增加
		 i18n.locale: "zh-CN"

注意：服务器上要开放外网访问端口，默认为 5601

```

### 下载安装 filebeat
```php
cd /usr/local/elk/
wget https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.6.0-linux-x86_64.tar.gz
tar xvf filebeat-7.6.0-linux-x86_64.tar.gz

```

#### 配置文件filebeat.yml
```php
# filebeat 输入标识
filebeat.inputs:

# Each - is an input. Most options can be set at the input level, so
# you can use different inputs for various configurations.
# Below are the input specific configurations.
# 定义输入类型，为日志类型
- type: log
  # Change to true to enable this input configuration.
  # 开启配置
  enabled: true

  # Paths that should be crawled and fetched. Glob based paths.
  # 读取日志文件罗京
  paths:
    - /usr/local/www/fastadmin/runtime/mylog/*/*.log
    - # 增加自定义字段
  fields:
     log_name: fits-links-log
  tail_files: true
    #- c:\programdata\elasticsearch\logs\*
- type: log
  enabled: true
  paths:
    - /usr/local/www/fastadmin/runtime/log/*/*.log
  fields:
      log_name: fits-icenter-log
  tail_files: true
  # Exclude lines. A list of regular expressions to match. It drops the lines that are
  # matching any regular expression from the list.
  #exclude_lines: ['^DBG']



```
问题2 怎么输出到 logstash中？
默认配置文件很给力，都有示例，太好用了
```php
#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  # Logstash 主机地址，我们默认本机的5044端口
  hosts: ["localhost:5044"]

  # Optional SSL. By default is off. 开启ssl
  # List of root certificates for HTTPS server verifications
  #ssl.certificate_authorities: ["/etc/pki/root/ca.pem"]

  # Certificate for SSL client authentication
  #ssl.certificate: "/etc/pki/client/cert.pem"

  # Client Certificate Key
  #ssl.key: "/etc/pki/client/cert.key"


```

配置后可以进行启动filebeat
```php
cd /usr/local/elk/filebeat-7.6.0-linux-x86_64
nohup ./filebeat -c filebeat.yml &

```

### 下载安装 logstash
```php
wget https://artifacts.elastic.co/downloads/logstash/logstash-7.6.0.tar.gz
tar xvf logstash-7.6.0.tar.gz

```

我们同样要考虑下面两个问题来进行配置
1.我们怎么读取日志输入？
通过filebeat把日志输入到logstash中，我们打开配置文件，进行下面配置

```php
cd /usr/local/elk/logstash-7.6.0/config
cp logstash-sample.conf logstash.conf
vim logstash.conf
//配置日志输入，beats配置
input {
  beats {
  	# beats的端口号
    port => 5044
    # 是否开启ssl加密，否
    ssl => false
  }
}


```

2.我们输出到哪里？
我们日志输出到elasticsearch中，上面说到此时有两个filebeats输入，我们怎么进行区分呢？
此时 filebeats中的fields字段此时就有很大用处了

```php
输出配置，输出到elasticsearch中
output {
  elasticsearch {
    # es的服务和端口
    hosts => ["http://localhost:9200"]
    # 输出到es中索引的创建格式，此时注意  {[fields][log_name]} ，这里使用了  filebeats中配置的自定义属性进行区分索引
    index => "%{[fields][log_name]}-%{+YYYY.MM.dd}"
    #index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
    #user => "elastic"
    #password => "changeme"
  }
}


```

这样我们很轻松就配置好了 logstash，下面进行启动服务

```php

cd /usr/local/elk/logstash-7.6.0/bin
nohup ./logstash -f ../config/logstash-es.conf &

```

通过上面操作，我们就成功配置和启动我们的elk日志系统了
下面开心的到kibana中使用下吧