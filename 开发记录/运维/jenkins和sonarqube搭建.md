### 1.安装最新版本jenkins
```php
 // 拉取最新的jenkins 镜像
 docker pull jenkins/jenkins:lts
 
 mkdir /home/jenkins_home;
 // 启动一个容器 
 docker run -d --name jenkins_01 -p 8081:8080  --restart=always \
        --env JAVA_OPTS="-Xms512m -Xmx512m -Xmn512m -XX:MaxNewSize=512m -Djava.util.logging.config.file=/var/jenkins_home/log.properties" \
         --env ES_JAVA_OPTS="-Xms256m -Xmx256m" \
       -v /home/jenkins_home:/home/jenkins_home jenkins/jenkins:lts ;

 // 进入jenkins_01 容器 复制密码
[root@localhost ~]#  docker exec -it jenkins_01 bash
jenkins@90e9c6e1a8d3:/$  cat /var/jenkins_home/secrets/initialAdminPassword
14aa8e172205****12fb96a79f4511e
jenkins@90e9c6e1a8d3:/$ 
// 推荐安装社区插件

// 安装插件 
SonarQube Scanner  
```


### 2. 安装分析工具sonarqube
```php

docker pull sonarqube:8.4.0-community
docker pull postgres:12.3-alpine
docker run --name postgresql -e POSTGRES_USER=sonar -e POSTGRES_PASSWORD=123456 -p 5432:5432 -v /data/postgresql/data:/var/lib/postgresql/data -d postgres:12.3-alpine

docker run -d --name sonarqube \
    --link postgresql \
    -p 9001:9000 \
    -e sonar.jdbc.url=jdbc:postgresql://postgresql:5432/sonar \
    -e sonar.jdbc.username=sonar \
    -e sonar.jdbc.password=123456 \
    -v /data/sonarqube/sonarqube_extensions:/opt/sonarqube/extensions \
    -v /data/sonarqube/sonarqube_logs:/opt/sonarqube/logs \
    -v /data/sonarqube/sonarqube_data:/opt/sonarqube/data \
    sonarqube:8.4.0-community

```

### 3. sonarqube 建立项目 配置token 提供给 jenkins 使用
1. 登录网址 http://192.168.31.204:9001/, 创建项目
![avatar](../../images/DevOps/sonarqube_1.png)

2. 创建令牌
![创建令牌](../../images/DevOps/sonarqube_2.png)

3. 令牌如下
![令牌如下](../../images/DevOps/sonarqube_3.png)

4. 安装中文插件和 php 分析代码插件
配置-> 应用市场 
找到 Chinese Pack 和 PHP Code Quality and Security 安装

### 4.登录jenkins 网址配置 sonarqube 

#### 创建项目
![创建项目](../../images/DevOps/jenkins_job_1.png)
![填写描述](../../images/DevOps/jenkins_job_2.png)
![源码](../../images/DevOps/jenkins_job_3.png)
![触发](../../images/DevOps/jenkins_job_4.png)
![触发](../../images/DevOps/jenkins_job_5.png)

```php
# ks-cms-unicorn 会显示在 sonarqube 项目里面
sonar.projectKey=ks-cms-unicorn
sonar.projectName=ks-cms-unicorn
sonar.projectVersion=1.0

sonar.language=java
sonar.sourceEncoding=UTF-8

sonar.sources=$WORKSPACE
sonar.java.binaries=$WORKSPACE
```

#### 系统配置
1. 系统管理-》系统配置
![系统配置](../../images/DevOps/jenkins_1.png)
![系统配置3](../../images/DevOps/jenkins_3.png)
![系统配置4](../../images/DevOps/jenkins_4.png)

2.系统管理->全局工具配置



![系统配置5](../../images/DevOps/jenkins_5.png)

