### 下载和安装lua
```php
wget http://luajit.org/download/LuaJIT-2.0.5.tar.gz 
tar -zxvf LuaJIT-2.0.5.tar.gz cd LuaJIT-2.0.5 
make && make install PREFIX=/usr/local/LuaJIT

vim /etc/profile
export LUAJIT_LIB=/usr/local/LuaJIT/lib 
export LUAJIT_INC=export LUAJIT_LIB=/usr/local/LuaJIT/lib

source /etc/profile
```