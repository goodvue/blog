# gdb 调试  php

### 步骤
1. php -v查看版本
2. 下载对应php版本的源码 [php](https://www.php.net/releases/)
3. gdb --pid=busy进程的pid
4. source php源码路径/.gdbinit，
5. zbacktrace 打印调用栈
6. 最后一步可以看到php代码当前执行的调用栈，也就是php代码死循环的位置。
注意：如果zbacktrace 没有打出调用栈，可能你的php编译时没有加入-g参数，需要重新编译php，然后重启workerman定位。


### 本地实践
1. 编写 workerman (里面有死循环)  
2. 下载对应的版本 php7.2.34
3. 进入 /home/test/7.2.34 
4. ./configure --enable-pcntl --enable-posix && make (不要make install 会覆盖本地php)
5. 进入  /home/test/php-7.2.34/sapi/cli 启动  php /home/test/test.php  start
6. google 浏览器 F12 
   ```js
       ws = new WebSocket("ws://192.168.31.211:2345");
       ws.send('d');

   ```
7. 进入  /home/test/php-7.2.34/sapi/cli 和  php /home/test/test.php status
8. gdb -pid 进程ID
9. 输入 pwd 命令 发现发目录  Working directory /home/test/php-7.2.34/sapi/cli. 是为了 source 使用
10.  source ../../.gdbinit
11. zbacktrace   发现代码 18 行 死循环
