<?php


ini_set('memory_limit','3872M');
require_once __DIR__ . '/vendor/autoload.php';

use Workerman\Worker;
use Workerman\Lib\Timer;
use Workerman\Connection\AsyncTcpConnection;
$worker = new Worker();
$worker->onWorkerStart = 'connect';
function connect(){
    static $count = 0;
    // 2000个链接
    if ($count++ >= 50000 ) return;
    // 建立异步链接
    $con = new AsyncTcpConnection('ws://127.0.0.1:7272');
    $con->onConnect = function($con) {
        // 递归调用connect
        connect();
    };
    $con->onMessage = function($con, $msg) {
        echo "recv $msg\n";
    };
    $con->onClose = function($con) {
        echo "con close\n";
    };
    // 当前链接每10秒发个心跳包
    Timer::add(60, function()use($con){
        $con->send("ping");
    });
    $con->connect();
    echo $count, " connections complete\n";
}
Worker::runAll();
