# apache_jemeter5.0 版本
### 使用介绍
1. 如果没有对应插件，则需要下载
2. 把插件加压放到 D:\软件\apache-jmeter-5.3\apache-jmeter-5.3\lib\ext
3. 重启



#### websocket 测试
1. 下载websocket 插件包 websocket-samplers-1.2.8
2. 创建线程组
3. 创建 WebSocket Open Connection
4. 创建 WebSocket request-response Sampler 可以使用 Open Connection 创建的websocket
   的连接
   
#### websocket 插件官方问题网站
1. https://bitbucket.org/pjtr/jmeter-websocket-samplers/issues/37/how-to-keep-alive-open-websocket   
   
### 测试websocket 打开连接和发送心跳包
1. 创建线程组   设置 10
2. 创建事务管理器
3. 创建websocket 打开连接
4. 创建 Loop Controller  设置 10次
5. 创建定时器  设置 2000 毫秒 = 2秒
6. 发送心跳连接

```php
    总结： 启动10个线程, 运行10次，每个线程每隔2秒发送一个心跳包
```
![websocket_ping](images/websocket_ping.png)

