### 一样的意思查询，性能却差异巨大

### 案例一：条件字段函数操作
```php
// 表结构 字段是索引的
CREATE TABLE `tradelog` (
  `id` int(11) NOT NULL,
  `tradeid` varchar(32) DEFAULT NULL,
  `operator` int(11) DEFAULT NULL,
  `t_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tradeid` (`tradeid`),
  KEY `t_modified` (`t_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

mysql> select count(*) from tradelog where month(t_modified)=7;

// 对索引字段做函数操作，可能会破坏索引值的有序性，因此优化器就决定放弃走树搜索功能。
```


### 案例二：隐式类型转换

```php
// tradeid 数据库保存的是 字符串， 现在跟 数字比较
select * from tradelog where tradeid=110717;
// 也会破坏破坏索引值的有序性 ，不走因此优化器就决定放弃走树搜索功能，全表搜索
```