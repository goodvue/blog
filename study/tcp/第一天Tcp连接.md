### 开启监听tcpdump 80

```php
// 查看ip
[root@master ~]# ifconfig
ens192: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.31.211  netmask 255.255.255.0  broadcast 192.168.31.255
        inet6 fe80::1c19:19ba:d012:82ce  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:df:9d:e4  txqueuelen 1000  (Ethernet)
        RX packets 64378611  bytes 5473784747 (5.0 GiB)
        RX errors 0  dropped 67  overruns 0  frame 0
        TX packets 45846881  bytes 4990897592 (4.6 GiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 92918665  bytes 66156980290 (61.6 GiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 92918665  bytes 66156980290 (61.6 GiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

// 查看监听

 [root@master ~]#tcpdump -nn -i ens192 port 80
 tcpdump: verbose output suppressed, use -v or -vv for full protocol decode

// 发送请求
[root@master ~]# curl www.baidu.com
<!DOCTYPE html>
<!--STATUS OK--><html> <head><meta http-equiv=content-type content=text/html;charset=utf-8>
<meta http-equiv=X-UA-Compatible content=IE=Edge><meta content=always name=referrer>
<link rel=stylesheet type=text/css href=http://s1.bdstatic.com/r/www/cache/bdorz/baidu.min.css><title>百度一下，你就知道</title></head> <body link=#0000cc> <div id=wrapper> <div id=head> <div class=head_wrapper> <div class=s_form> <div class=s_form_wrapper> <div id=lg> <img hidefocus=true src=//www.baidu.com/img/bd_logo1.png width=270 height=129> </div> <form id=form name=f action=//www.baidu.com/s class=fm> <input type=hidden name=bdorz_come value=1> <input type=hidden name=ie value=utf-8> <input type=hidden name=f value=8> <input type=hidden name=rsv_bp value=1> <input type=hidden name=rsv_idx value=1> <input type=hidden name=tn value=baidu><span class="bg s_ipt_wr"><input id=kw name=wd class=s_ipt value maxlength=255 autocomplete=off autofocus></span><span class="bg s_btn_wr"><input type=submit id=su value=百度一下 class="bg s_btn"></span> </form> </div> </div> <div id=u1> <a href=http://news.baidu.com name=tj_trnews class=mnav>新闻</a> <a href=http://www.hao123.com name=tj_trhao123 class=mnav>hao123</a> <a href=http://map.baidu.com name=tj_trmap class=mnav>地图</a> <a href=http://v.baidu.com name=tj_trvideo class=mnav>视频</a> <a href=http://tieba.baidu.com name=tj_trtieba class=mnav>贴吧</a> <noscript> <a href=http://www.baidu.com/bdorz/login.gif?login&amp;tpl=mn&amp;u=http%3A%2F%2Fwww.baidu.com%2f%3fbdorz_come%3d1 name=tj_login class=lb>登录</a> </noscript> <script>document.write('<a href="http://www.baidu.com/bdorz/login.gif?login&tpl=mn&u='+ encodeURIComponent(window.location.href+ (window.location.search === "" ? "?" : "&")+ "bdorz_come=1")+ '" name="tj_login" class="lb">登录</a>');</script> <a href=//www.baidu.com/more/ name=tj_briicon class=bri style="display: block;">更多产品</a> </div> </div> </div> <div id=ftCon> <div id=ftConw> <p id=lh> <a href=http://home.baidu.com>关于百度</a> <a href=http://ir.baidu.com>About Baidu</a> </p> <p id=cp>&copy;2017&nbsp;Baidu&nbsp;<a href=http://www.baidu.com/duty/>使用百度前必读</a>&nbsp; <a href=http://jianyi.baidu.com/ class=cp-feedback>意见反馈</a>&nbsp;京ICP证030173号&nbsp; <img src=//www.baidu.com/img/gs.gif> </p> </div> </div> </div> </body> </html>
[root@hongmaster ~]# 

// 查看三次握手 和 四次分手
 [root@master ~]#tcpdump -nn -i ens192 port 80
 tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
// 三次握手
// 第一次 客户端 跟百度连接 发送 SYN
02:25:53.686467 IP 192.168.31.211.33223 > 14.215.177.38.80: Flags [S], seq 1489050956, win 29200, options [mss 1460,sackOK,TS val 601180068 ecr 0,nop,wscale 7], length 0
// 第二次 百度 发送 SNY + ack  到 客户端 （百度说 跟我连接，那我发送ack ,是否能收到）
02:25:53.694486 IP 14.215.177.38.80 > 192.168.31.211.33223: Flags [S.], seq 343928642, ack 1489050957, win 8192, options [mss 1400,sackOK,nop,nop,nop,nop,nop,nop,nop,nop,nop,nop,nop,wscale 5], length 0
// 第二次 客户端 发送 ACK 到 百度 （我收到 ack 正式连接连接  就是可靠了）
02:25:53.694533 IP 192.168.31.211.33223 > 14.215.177.38.80: Flags [.], ack 1, win 229, length 0
// 请求和接收数据
02:25:53.694672 IP 192.168.31.211.33223 > 14.215.177.38.80: Flags [P.], seq 1:78, ack 1, win 229, length 77: HTTP: GET / HTTP/1.1
02:25:53.703644 IP 14.215.177.38.80 > 192.168.31.211.33223: Flags [.], ack 78, win 908, length 0
02:25:53.706785 IP 14.215.177.38.80 > 192.168.31.211.33223: Flags [.], seq 1:1401, ack 78, win 908, length 1400: HTTP: HTTP/1.1 200 OK
02:25:53.706805 IP 192.168.31.211.33223 > 14.215.177.38.80: Flags [.], ack 1401, win 251, length 0
02:25:53.706823 IP 14.215.177.38.80 > 192.168.31.211.33223: Flags [P.], seq 1401:2782, ack 78, win 908, length 1381: HTTP
02:25:53.706833 IP 192.168.31.211.33223 > 14.215.177.38.80: Flags [.], ack 2782, win 274, length 0
// 四次分手 分手
// 第一次 客户端 发送 Fin ack我要跟百度断开连接
02:25:53.707098 IP 192.168.31.211.33223 > 14.215.177.38.80: Flags [F.], seq 78, ack 2782, win 274, length 0
// 第二次 百度 会应 ack 好的
02:25:53.715461 IP 14.215.177.38.80 > 192.168.31.211.33223: Flags [.], ack 79, win 908, length 0
// 第三次 百度 会应 Fin ack 断开
02:25:53.715490 IP 14.215.177.38.80 > 192.168.31.211.33223: Flags [F.], seq 2782, ack 79, win 908, length 0
// 第四次 客户端 收到断开 ack 回应 服务端
02:25:53.715512 IP 192.168.31.211.33223 > 14.215.177.38.80: Flags [.], ack 2783, win 274, length 0



```

#### 为什么要 三次握手和四次分手