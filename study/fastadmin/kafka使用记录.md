### 本地环境是phpstudy 配置kafka 环境
1 . 选择kafka扩展
```php
PHP Version 7.2.1
Architecture	x86
PHP Extension Build	API20170718,NTS,VC15
// 所以选择对应的kafka 扩展下载 
https://windows.php.net/downloads/pecl/releases/rdkafka/4.1.2/php_rdkafka-4.1.2-7.2-nts-vc15-x64.zip
```

2 . 安装插件
```php
复制 librdkafka.dll 到D:\phpStudy\PHPTutorial\php\php-7.2.1-nts\目录下

复制php_rdkafka.dll放到 D:\phpStudy\PHPTutorial\php\php-7.2.1-nts\ext目录下

在 php.ini 文件中添加 extension=php_rdkafka.dll

```

### 代码实现

// 搞了半天代码没有找到 RdKafka类 原因是没有引入代码
External Libraries / PHP Runtime

![配置如图](../../images/php/phpstrom_code.png)

```php
<?php


namespace app\index\controller;

use app\common\controller\Frontend;
use RdKafka\Conf;
use RdKafka\Producer;

class Kafkaproducer extends Frontend
{
    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';



    public function index()
    {

        $conf = new Conf();
        $conf->set('metadata.broker.list', '192.168.31.1:9092');

//If you need to produce exactly once and want to keep the original produce order, uncomment the line below
//$conf->set('enable.idempotence', 'true');

        $producer = new Producer($conf);

        $topic = $producer->newTopic("student");

        for ($i = 0; $i < 10; $i++) {
            $topic->produce(RD_KAFKA_PARTITION_UA, 0, "Message nihao $i");
            $producer->poll(0);
        }

        return "发送消息完毕";

    }

    public function consumer()
    {
       $rk = new Consumer();
       $rk->setLogLevel(LOG_DEBUG);
       // 指定 broker 地址,多个地址用"," 分割
       $rk->addBrokers("192.168.31.1:9092");
       
       
       $topic = $rk->newTopic("test");
       $topic->consumeStart(0, RD_KAFKA_OFFSET_BEGINNING);
       
       
       while (true) {
           // 第一个参数是分区号
           // 第二个参数是超时时间
           $msg = $topic->consume(0, 1000);
           if ($msg->err) {
               echo $msg->errstr(), "\n";
               break;
           } else {
               echo $msg->payload, "\n";
           }
       }
    }
}
```