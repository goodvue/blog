># Dcoker-Compose

>#### <font face="微软雅黑"  color = #42A5F5 >Compose介绍? </font> 

Compose是 Docker  的服务编排工具，主要用来构建基于Docker的复杂应用Compose 通过一个配置文件来管理多个 -Docker  容器，非常适合组合使用多个容器进行开发的场景。

Compose 中有两个重要的概念：

-  服务 (service) ：一个应用的容器，实际上可以包括若干运行相同镜像的容器实例。
-  项目 (project) ：由一组关联的应用容器组成的一个完整业务单元，在 docker-compose.yml 文件中定义。

一个项目可以由多个服务（容器）关联而成，Compose 面向项目进行管理，通过子命令对项目中的一组容器进行便捷地生命周期管理。

&ensp;&ensp;例如，你有一个 **```php```** 镜像，一个 **```redis```** 镜像，一个**```nginx```**镜像。如果没有 **```docker-compose```**，那么每次启动的时候，你需要敲各个容器的启动参数，环境变量，容器命名，指定不同容器的链接参数等等一系列的操作，相当繁琐。  

而用了 **```docker-composer ```**之后，你就可以把这些命令一次性写在 **```docker-composer.yml```** 文件中，以后每次启动这一整个环境（含3个容器）的时候，你只要敲一个 **```docker-composer```** up命令就ok了。

**```dockerfile```**的作用是从无到有的构建镜像。它包含安装运行所需的环境、程序代码等。这个创建过程就是使用 **```dockerfile```** 来完成的。 
   
Dockerfile -为 **```docker build```** 命令准备的，用于建立一个独立的 **```image```** ，在 **```docker-compose```** 里也可以用来实时 **```build```**   
> #### <font face="微软雅黑"  color = #42A5F5 > 安装composer </font> 


##### <font face="微软雅黑"  color = #42A5F5 > Curl方式下载新的版本 </font>
```
curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose  
```  

修改权限

```
chmod +x /usr/local/bin/docker-compose
```

安装完成后可以查看版本：
  
```docker-compose --version```  

一般步骤
 
```
1、定义Dockerfile，方便迁移到任何地方；
2、编写docker-compose.yml文件；
3、运行docker-compose up启动服务
```
示例：
准备工作：提前下载好镜像：

>需要新建一个空白目录，在目录新建一个 ```docker-compose.yml```

![](性能优化-docker-compose_files/2.jpg)

以上命令的意思是新建nginx和redis容器。  

好，我们启动应用：  

**```docker-compose up```**

就成功了。浏览器访问 即可。
默认是前台运行并打印日志到控制台。如果想后台运行，可以： 

**```docker-compose up –d```**

![](性能优化-docker-compose_files/1.jpg)

>服务后台后，可以使用下列命令查看状态：  

**```docker-compose ps```**

>停止服务：  

**```docker-compose stop```**

>重新启动服务：

**```docker-compose restart```**



#### <font face="微软雅黑"  color = #42A5F5 >docker-compose.yml模版文件常用命令</font>

docker-compose的默认模版文件为：docker-compose.yml。和Dockerfile一样，它也是有自己的语法命令的。其中定义的每个服务都必须通过image指令指定镜像或build指令(需要 Dockerfile)来自动构建。其它大部分指令都跟docker run中的类似。


模版文件有多种写法,下图是Compose和Docker兼容性说明：

![](性能优化-docker-compose_files/4.jpg)


每个docker-compose.yml必须定义image或者build中的一个，其它的是可选的。
image
指定镜像tag或者ID。示例：   

![](性能优化-docker-compose_files/5.jpg)

**```Build```**
用来指定一个包含Dockerfile文件的路径。一般是当前目录.。Fig将build并生成一个随机命名的镜像。

![](性能优化-docker-compose_files/6.jpg)

**```context```**为路径，**```dockerfile```**为需要替换默认**```docker-compose```**的文件名，**```args```**为构建(build)过程中的环境变量，用于替换**```Dockerfile```**里定义的ARG参数，容器中不可用。示例：

>### <font face="微软雅黑"  color = #42A5F5 > Dockerfile: </font>

![](性能优化-docker-compose_files/7.jpg)

docker-compose.yml:  

![](性能优化-docker-compose_files/8.jpg)


####  Command  
用来覆盖缺省命令。示例：

```command: bundle exec thin -p 3000```  

![](性能优化-docker-compose_files/9.jpg)

command也支持数组形式： 

```command: [bundle, exec, thin, -p, 3000]```  
![](性能优化-docker-compose_files/10.jpg)


####  Links  
用于链接另一容器服务，如需要使用到另一容器的mysql服务。可以给出服务名和别名；也可以仅给出服务名，这样别名将和服务名相同。同 ```docker run --link```。  
示例：  
![](性能优化-docker-compose_files/11.jpg)

使用了别名将自动会在容器的 ```/etc/hosts``` 文件里创建相应记录：  
![](性能优化-docker-compose_files/12.jpg)

所以我们在容器里就可以直接使用别名作为服务的主机名。

####  Ports  

用于暴露端口。同docker run -p。示例：  

![](性能优化-docker-compose_files/13.jpg)


####  Expose  

expose提供container之间的端口访问，不会暴露给主机使用。同docker run --expose。
![](性能优化-docker-compose_files/14.jpg)

####  volumes    

挂载数据卷。同 ```docker run -v```。示例：  

![](性能优化-docker-compose_files/15.jpg)

#### environment  

添加环境变量。同 ```docker run -e```。可以是数组或者字典格式：
![](性能优化-docker-compose_files/17.jpg)

#### depends_on  

用于指定服务依赖，一般是mysql、redis等。
指定了依赖，将会优先于服务创建并启动依赖。

![](性能优化-docker-compose_files/22.jpg)

#### extra_hosts 添加主机名映射。  
![](性能优化-docker-compose_files/19.jpg)  
将会在/etc/hosts创建记录：

![](性能优化-docker-compose_files/20.jpg)  


#### networks

一些场景下，默认的网络配置满足不了我们的需求，此时我们可使用networks命令自定义网络
加入指定网络，格式如下：

![](性能优化-docker-compose_files/18.jpg)


#### Dns
自定义dns服务器。   

![](性能优化-docker-compose_files/23.jpg)


还有很多命令，但是通常使用并不是那么多，如果项目当中需要可以通过参考官方手册去查看
