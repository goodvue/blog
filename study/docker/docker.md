第二种：下载安装 docker-compose

1、运行命令，下载 Docker Compose：

curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
2、加权限：

chmod +x /usr/local/bin/docker-compose
3、添加软连接：

ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose